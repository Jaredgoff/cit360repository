package Pizzas;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Pizzaservlet", urlPatterns={"/PizzaServlet"})
public class Pizzaservlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String placeorder = request.getParameter("placeorder");
        String placeorder2 = request.getParameter("placeorder2");
        out.println("<h1>Order Your Pizza Here</h1>");
        out.println("<p> Kiosk1: " + placeorder + " </p>");
        out.println("<p> Kiosk2: " + placeorder2 + " </p>");
        out.println("</body></html>");
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available at this time.");
}
