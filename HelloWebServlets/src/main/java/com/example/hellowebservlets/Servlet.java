package com.example.hellowebservlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String place1 = request.getParameter("place1");
        String action = request.getParameter("action");
        String place2 = request.getParameter("place2");
        out.println("<h1>Launch Nuclear Weapons</h1>");
        out.println("<p> From " + place1 + " </p>");
        out.println("<p>" + action + " </p>");
        out.println("<p>to hit " + place2 + " </p>");
        out.println("</body></html>");
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available at this time.");
    }
}
