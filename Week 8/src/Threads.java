public class Threads implements Runnable {

    private String name;
    private int speed;
    private int miles;

    public Threads(String name, int speed, int miles) {
        this.name = name;
        this.speed = speed;
        this.miles = miles;
    }


    public void run() {
        System.out.println("\n\nExecuting with these parameters: Name =" + name + " Speed = "
                + speed + " Miles = " + miles + " \n\n");
        for (int count = 1; count < miles; count++) {
            if (count % speed == 0) {
                System.out.print(name + " speed. ");
                try {
                    Thread.sleep(speed);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        /*System.out.println("How fast can " + name +"'s go " + speed + "mph and went this many miles " + miles + ".\n\n");

        for (int i = 1; i < 7; i++){
            if ( i % speed == 1){
                System.out.println(name + "'s turn.");
                try{
                    Thread.sleep(speed);
                }
                catch (InterruptedException ex){
                    System.out.println("Program was interrupted.");
                }*/


        }
        /*System.out.println(name + "'is the next car.");*/
        System.out.println("\n\n" + name + " is done.\n\n");
    }

}
