import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThread {
    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        Threads tp1 = new Threads("Mustang", 150, 8);
        Threads tp2 = new Threads("Camero", 130, 6);
        Threads tp3 = new Threads("RX7", 120, 5);
        Threads tp4 = new Threads("Accord", 110, 4);
        Threads tp5 = new Threads("Civic", 100, 3);
        Threads tp6 = new Threads("Sonata", 60, 2);

        myService.execute(tp1);
        myService.execute(tp2);
        myService.execute(tp3);
        myService.execute(tp4);
        myService.execute(tp5);
        myService.execute(tp6);

        myService.shutdown();
    }
}
